<!DOCTYPE html>
<html lang="en">
<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="UTF-8">
    <meta name="description" content="Evento -Event Html Template">
    <meta name="keywords" content="Evento , Event , Html, Template">
    <meta name="author" content="ColorLib">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- ========== Title ========== -->
    <title> Semout (Kasir )</title>
    <!-- ========== Favicon Ico ========== -->
    <!--<link rel="icon" href="fav.ico">-->
    <!-- ========== STYLESHEETS ========== -->
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url("resources/assets/css/bootstrap.min.css") ?>" rel="stylesheet">
    <!-- Fonts Icon CSS -->
    <link href="<?php echo base_url("resources/assets/css/font-awesome.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("resources/assets/css/et-line.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("resources/assets/css/ionicons.min.css") ?>" rel="stylesheet">
    <!-- Carousel CSS -->
    <link href="<?php echo base_url("resources/assets/css/owl.carousel.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("resources/assets/css/owl.theme.default.min.css") ?>" rel="stylesheet">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url("resources/assets/css/animate.min.css") ?>">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url("resources/assets/css/main.css") ?>" rel="stylesheet">
</head>
<body>
<div class="loader">
    <div class="loader-outter"></div>
    <div class="loader-inner"></div>
</div>

<!--header start here -->
<header class="header navbar fixed-top navbar-expand-md">
    <div class="container">
        <a class="navbar-brand logo" href="#">
            <img src="<?php echo base_url("resources/assets/img/logoSemout.png") ?>" alt="SEMOUT"/>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headernav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="lnr lnr-text-align-right"></span>
        </button>
        <div class="collapse navbar-collapse flex-sm-row-reverse" id="headernav">
            <ul class=" nav navbar-nav menu">
                <li class="nav-item">
                    <a class="nav-link active" href="#">Home</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link active" href="<?php //echo base_url("dokumentasi") ?>">Installasi</a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link active" href="<?php echo base_url("register") ?>">Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="<?php echo base_url("login") ?>">Login</a>
                </li>
                
                <li class="search_btn">
                    <a  href="#">
                       <i class="ion-ios-search"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</header>
<!--header end here-->

<!--cover section slider -->
<section id="home" class="home-cover">
    <div class="cover_slider owl-carousel owl-theme">
        <div class="cover_item" style="background: url('<?php echo base_url("resources/assets/img/bg/1.jpg") ?>');">
             <div class="slider_content">
                <div class="slider-content-inner">
                    <div class="container">
                    <div class="slider-content-left">
                            <h2 class="cover-title">
                            Dapatkan Aplikasi Semout Kasir Dengan Gratis
                            </h2>
                            <strong class="cover-xl-text">Semout Kasir</strong>
                            <p class="cover-date">
                            Sampai Desember 2020
                            </p>
                            <a href="<?= $link_download;?>" class=" btn btn-primary btn-rounded" >
                            Daftar dan Download Sekarang
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cover_item" style="background: url('<?php echo base_url("resources/assets/img/bg/2.jpg")?>');">
            <div class="slider_content">
                <div class="slider-content-inner">
                    <div class="container">
                        <div class="slider-content-left">
                        <h2 class="cover-title">
                            Dapatkan Aplikasi Semout Kasir Dengan Gratis
                            </h2>
                            <strong class="cover-xl-text">Semout Kasir</strong>
                            <p class="cover-date">
                            Sampai Desember 2020
                            </p>
                            <a href="<?= $link_download;?>" class=" btn btn-primary btn-rounded" >
                            Daftar dan Download Sekarang
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cover_item" style="background: url('<?php echo base_url("resources/assets/img/bg/3.png")?>');">
            <div class="slider_content">
                <div class="slider-content-inner">
                    <div class="container">
                        <div class="slider-content-center">
                        <h2 class="cover-title">
                                Dapatkan Aplikasi Semout Kasir Dengan Gratis
                            </h2>
                            <strong class="cover-xl-text">Semout Kasir</strong>
                            <p class="cover-date">
                                Sampai Desember 2020
                            </p>
                            <a href="<?= $link_download;?>" class=" btn btn-primary btn-rounded" >
                                Daftar dan Download Sekarang
                            </a>
                           </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="cover_nav">
        <ul class="cover_dots">
            <li class="active" data="0"><span>1</span></li>
            <li  data="1"><span>2</span></li>
            <li  data="2"><span>3</span></li>
        </ul>
    </div>
</section>
<!--cover section slider end -->

<!--event info 
<section class="pt100 pb100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 col-md-3  ">
                <div class="icon_box_two">
                    <i class="ion-ios-calendar-outline"></i>
                    <div class="content">
                        <h5 class="box_title">
                            DATE
                        </h5>
                        <p>
                            12-14 february 2018
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-3  ">
                <div class="icon_box_two">
                    <i class="ion-ios-location-outline"></i>
                    <div class="content">
                        <h5 class="box_title">
                            location
                        </h5>
                        <p>
                            Los Angeles, CA.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-3  ">
                <div class="icon_box_two">
                    <i class="ion-ios-person-outline"></i>
                    <div class="content">
                        <h5 class="box_title">
                            speakers
                        </h5>
                        <p>
                            Natalie James
                            + guests
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-3  ">
                <div class="icon_box_two">
                    <i class="ion-ios-pricetag-outline"></i>
                    <div class="content">
                        <h5 class="box_title">
                            tikets
                        </h5>
                        <p>
                            $65 early bird
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
event info end -->


<!--event countdown -->
<section class="bg-img pt70 pb70" style="background-image: url('<?php echo base_url("resources/assets/img/bg/bg-img.png")?>');">
    <div class="overlay_dark"></div>
    <div class="container">
        <div class="row justify-content-center">
                       
            <div class="col-12 col-md-12">
                <h4 class="mb30 text-center color-light">Ayo Segera Daftar dan Download Aplikasi Kasir  Android  <strong>Semout</strong> untuk Umkm anda! </h4>
                <!-- <div class="countdown"></div> -->
                <center><a href='https://play.google.com/store/apps/details?id=com.semout.framework.app&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>
                </center>
            </div>
        </div>
    </div>
</section>
<!--event count down end-->


<!--about the event -->
<section class="pt100 pb100">
    <div class="container">
        <div class="section_title">
            <h3 class="title">
                Fitur Semout Kasir
            </h3>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing eli. Integer iaculis in lacus a sollicitudin. Ut hendrerit hendrerit nisl a accumsan. Pellentesque convallis consectetur tortor id placerat. Curabitur a pulvinar nunc. Maecenas laoreet finibus lectus, at volutpat ligula euismod.
                </p>
            </div>
            <div class="col-12 col-md-6">
                <p>
                    In rhoncus massa nec  sollicitudin. Ut hendrerit hendrerit nisl a accumsan. Pellentesque convallis consectetur tortor id placerat. Curabitur a pulvinar nunc. Maecenas laoreet finibus lectus, at volutpat ligula euismod quis. Maecenas ornare, ex in malesuada tempus.
                </p>
            </div>
        </div>

        <!--event features-->
        <div class="row justify-content-center mt30">
            <div class="col-12 col-md-6 col-lg-3">
                <div class="icon_box_one">
                    <i class="lnr lnr-mic"></i>
                    <div class="content">
                        <h4>Kasir POS</h4>
                        <p>
                            Kasir <strong>Semout</strong> memiliki fitur kasir dengan berbagai tampilan layout sesuai kebutuhan
                            <li>Restoran</li>
                            <li>Toko Kelontong</li>
                            <li>Supermarket</li>
                            <li>Dan untuk penjual di pasar</li>
                        </p>
                        <a href="#">read more</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-3">
                <div class="icon_box_one">
                    <i class="lnr lnr-rocket"></i>
                    <div class="content">
                        <h4>Laporan</h4>
                        <p>
                            
                        </p>
                        <a href="#">read more</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-3">
                <div class="icon_box_one">
                    <i class="lnr lnr-bullhorn"></i>
                    <div class="content">
                        <h4>Info Stok </h4>
                        <p>
                            Untuk stok pengguna dapat memantau stok barang yang terjual serta dapat memanagement stok seperti cek untuk kondisi stok rusak / stok hilang serta masih banyak fitur lainya
                        </p>
                        <a href="#">read more</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-3">
                <div class="icon_box_one">
                    <i class="lnr lnr-clock"></i>
                    <div class="content">
                        <h4>Aman Mudah dan Murah </h4>
                        <p>
                            Semout kasir menyasar pada pasar Usaha kecil micro menengah (UMKM) dengan dukungan sistem dan pengalaman dalam transaksi kasir insyallah kebutuhan transaksi anda dapat terpenuhi dan termanage dengan baik. 
                        </p>
                        <a href="#">read more</a>
                    </div>
                </div>
            </div>
        </div>
        <!--event features end-->
    </div>
</section>
<!--about the event end -->


<!--event countdown -->
<section class="bg-img pt70 pb70" style="background-image: url('<?php echo base_url("resources/assets/img/bg/bg-img.png")?>');">
    <div class="overlay_dark"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10">
                <!-- <h4 class="mb30 text-center color-light">Ayo Segera Daftar dan Download Aplikasi Kasir <strong>Semout</strong> untuk Umkm anda!</h4> -->
                <!-- <div class="countdown"></div> -->
            </div>
        </div>
    </div>
</section>
<!--event count down end-->

<!--Price section-->
<section class="pb100">
    <div class="container">
        <div class="section_title mb50">
            
        </div>

        <div class="row justify-content-center">
            <div class="col-md-4 col-12">
                <div class="price_box active">
                    
                   <div class="price_header">
                       <h4>
                           Promosi
                       </h4>
                       <h6>
                            Segera Coba 
                       </h6>
                   </div>
                    <div class="price_tag">
                        0 <sup>Rupiah</sup>
                    </div>
                    <div class="price_features">
                        <ul>
                            <li>
                                Kasir POS
                            </li>
                            <li>
                                Kasir Instant Untuk Pedagang Pasar New*
                            </li>
                            <li>
                                Management Produk
                            </li>
                            <li>
                                Management Stok Basic*
                            </li>
                            <li>
                                Management Pembelian Barang Toko(Kulakan/Restock)
                            </li>
                            <li>
                                Laporan Penjualan Basic*
                            </li>
                        </ul>
                    </div>
                    <div class="price_footer">
                        <a href="#" class="btn btn-success btn-rounded">Install Gratis</a>
                    </div>
                </div>
            </div>
             <div class="col-md-4 col-12">
                <div class="price_box">
                <div class="price_highlight">
                        recommended
                    </div>
                    <div class="price_header">
                        <h4>
                            Berbayar
                        </h4>
                        <h6>
                            
                        </h6>
                    </div>
                    <div class="price_tag">
                    <!-- <sup>Rupiah</sup> -->
                    </div>
                    <div class="price_features">
                        <ul>
                            <li>
                                Laporan Keuntungan
                            </li>
                            <li>
                                Penggajian Pegawai
                            </li>
                            <li>
                                
                            </li>
                            <li>
                                
                            </li>
                        </ul>
                    </div>
                    <div class="price_footer">
                        <a href="#" class="btn btn-success btn-rounded">Upgrade</a>
                    </div>
                </div>
            </div>
            <!--
            <div class="col-md-4 col-12">
                <div class="price_box">
                    <div class="price_header">
                        <h4>
                            Corporate
                        </h4>
                        <h6>
                            For the business
                        </h6>
                    </div>
                    <div class="price_tag">
                        95 <sup>$</sup>
                    </div>
                    <div class="price_features">
                        <ul>
                            <li>
                                Early Entrance
                            </li>
                            <li>
                                Front seat
                            </li>
                            <li>
                                Complementary Drinks
                            </li>
                            <li>
                                Promo Gift
                            </li>
                        </ul>
                    </div>
                    <div class="price_footer">
                        <!-- <a href="#" class="btn btn-primary btn-rounded">Purchase</a> -->
                    </div>
                </div>
            </div> 
        </div>
    </div>
</section>
<!--price section end -->

<!--event calender end -->

<!--brands section -->
<section class="bg-gray pt100 pb100">
    <div class="container">
        <div class="section_title mb50">
            <h3 class="title">
                Partner Kita
            </h3>
        </div>
        <div class="brand_carousel owl-carousel">
           
        </div>
    </div>
</section>
<!--brands section end-->

<!--get tickets section -->
<section class="bg-img pt100 pb100" style="background-image: url('<?php echo base_url("resources/assets/img/bg/tickets.png")?>');">
    <div class="container">
        <div class="section_title mb30">
            <h3 class="title color-light">
                Dapatkan
            </h3>
        </div>
        <div class="row justify-content-center align-items-center">
            <!-- <div class="col-md-9 text-md-left text-center color-light">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rhoncus massa nec gravida tempus. Integer iaculis in aazzzCurabitur a pulvinar nunc. Maecenas laoreet finibus lectus, at volutpat ligula euismod.
            </div> -->
            <div class="col-md-3 text-md-right text-center">
                <a href="#" class="btn btn-primary btn-rounded mt30">Download Gratis</a>
            </div>
        </div>
    </div>
</section>
<!--get tickets section end-->

<!--footer start -->
<footer>
    <div class="container">
        <div class="row justify-content-center">

      

           
        </div>
    </div>
</footer>
<div class="copyright_footer">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-12">
                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
</div>
<!--footer end -->

<!-- jquery -->
<script src="<?php echo base_url("resources/assets/js/jquery.min.js")?>"></script>
<!-- bootstrap -->
<script src="<?php echo base_url("resources/assets/js/popper.js")?>"></script>
<script src="<?php echo base_url("resources/assets/js/bootstrap.min.js")?>"></script>
<script src="<?php echo base_url("resources/assets/js/waypoints.min.js")?>"></script>
<!--slick carousel -->
<script src="<?php echo base_url("resources/assets/js/owl.carousel.min.js")?>"></script>
<!--parallax -->
<script src="<?php echo base_url("resources/assets/js/parallax.min.js")?>"></script>
<!--Counter up -->
<script src="<?php echo base_url("resources/assets/js/jquery.counterup.min.js")?>"></script>
<!--Counter down -->
<script src="<?php echo base_url("resources/assets/js/jquery.countdown.min.js")?>"></script>
<!-- WOW JS -->
<script src="<?php echo base_url("resources/assets/js/wow.min.js")?>"></script>
<!-- Custom js -->
<script src="<?php echo base_url("resources/assets/js/main.js")?>"></script>
</body>
</html>