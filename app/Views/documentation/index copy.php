<!DOCTYPE html>
<html lang="en">

<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="UTF-8">
    <meta name="description" content="Aplikasi Kasir">
    <meta name="keywords" content="Pos,Mobile Kasir,Solusi Kasir">
    <meta name="author" content="ColorLib">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- ========== Title ========== -->
    <title> Kakao (Kasir )</title>
    <!-- ========== Favicon Ico ========== -->
    <!--<link rel="icon" href="fav.ico">-->
    <!-- ========== STYLESHEETS ========== -->
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url("resources/assets/css/bootstrap.min.css") ?>" rel="stylesheet">
    <!-- Fonts Icon CSS -->
    <link href="<?php echo base_url("resources/assets/css/font-awesome.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("resources/assets/css/et-line.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("resources/assets/css/ionicons.min.css") ?>" rel="stylesheet">
    <!-- Carousel CSS -->
    <link href="<?php echo base_url("resources/assets/css/owl.carousel.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("resources/assets/css/owl.theme.default.min.css") ?>" rel="stylesheet">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url("resources/assets/css/animate.min.css") ?>">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url("resources/assets/css/main.css") ?>" rel="stylesheet">
</head>

<body>
    <div class="loader">
        <div class="loader-outter"></div>
        <div class="loader-inner"></div>
    </div>

    <!--header start here -->
    <header class="header navbar fixed-top navbar-expand-md">
        <div class="container">
            <a class="navbar-brand logo" href="#">
                <img src="<?php echo base_url("resources/assets/img/logomobi.png") ?>" alt="Evento" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headernav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="lnr lnr-text-align-right"></span>
            </button>
            <div class="collapse navbar-collapse flex-sm-row-reverse" id="headernav">
                <ul class=" nav navbar-nav menu">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url("/") ?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="<?php echo base_url("dokumentasi") ?>">Installasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url("register") ?>">Register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="<?php echo base_url("login") ?>">Login</a>
                    </li>

                    <li class="search_btn">
                        <a href="#">
                            <i class="ion-ios-search"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <!--header end here-->

    <!--events section -->
    <section class="pt100 pb100">
        <div class="container">

            <div class="event_box">
                <!-- <img src="assets/img/events/event1.png" alt="event"> -->
                <div class="event_info">
                    <div class="event_title">
                        Panduan Instalasi
                    </div>
                    <div class="speakers">
                        <strong>1. XAMPP</strong>
                        <br>
                        <img src="<?= base_url('resources/dist/img/xampp_screenshoot.png') ?>" alt="event">
                        <p>Download Aplikasi Xampp Untuk Windows <a href="https://www.apachefriends.org/xampp-files/7.3.23/xampp-windows-x64-7.3.23-0-VC15-installer.exe" style="
    color: red;font-style: oblique;
">Disini</a>
                        </p>
                        <p>Setelah selesai mendownload kemudian klik <b>xampp-windows-x64-7.3.23-0-VC15-installer.exe</b> di folder Download Klik Install tunggu sampai selesai</p>
                    </div>
                    <article class="article-post">

                        </ul>
                        <h2><span id="Berikut_ini_adalah_langkah_cara_instal_XAMPP"><strong>Berikut ini adalah langkah cara instal XAMPP …</strong></span></h2>
                        <h3><span id="1_Langkah_1_Unduh_XAMPP"><b>1. Langkah 1: Unduh XAMPP</b></span></h3>
                        <p>Download XAMPP melalui website Apache Friends <span style="color: #3366ff;"><a style="color: #3366ff;" href="https://www.apachefriends.org/index.html" target="_blank" rel="noopener noreferrer nofollow"><b>berikut ini</b></a></span>.</p>
                        <p><img class="aligncenter wp-image-3687 lazyloaded" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp.png" alt="cara instal xampp" width="650" height="400" sizes="(max-width: 650px) 100vw, 650px" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp.png 1019w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-300x185.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-768x473.png 768w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-860x529.png 860w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-680x418.png 680w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-500x308.png 500w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-400x246.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-200x123.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-50x31.png 50w" data-ll-status="loaded"><noscript><img class="aligncenter wp-image-3687" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp.png" alt="cara instal xampp" width="650" height="400" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp.png 1019w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-300x185.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-768x473.png 768w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-860x529.png 860w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-680x418.png 680w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-500x308.png 500w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-400x246.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-200x123.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-50x31.png 50w" sizes="(max-width: 650px) 100vw, 650px"></noscript></p>
                        <h3><span id="2_Langkah_2_Instal_XAMPP"><b>2. Langkah 2: Instal XAMPP</b></span></h3>
                        <p>1. Lakukan instalasi setelah Anda selesai mengunduh. Selama proses instalasi mungkin Anda akan melihat pesan yang menanyakan apakah Anda yakin akan menginstalnya. Silakan tekan <b>Yes</b> untuk melanjutkan instalasi.</p>
                        <p>2. Klik tombol <b>Next</b>.</p>
                        <p><img class="aligncenter wp-image-3690 size-full lazyloaded" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows.png" alt="instal xampp di windows" width="501" height="414" sizes="(max-width: 501px) 100vw, 501px" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows.png 501w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-300x248.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-500x413.png 500w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-400x331.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-200x165.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-50x41.png 50w" data-ll-status="loaded"><noscript><img class="aligncenter wp-image-3690 size-full" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows.png" alt="instal xampp di windows" width="501" height="414" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows.png 501w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-300x248.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-500x413.png 500w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-400x331.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-200x165.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-50x41.png 50w" sizes="(max-width: 501px) 100vw, 501px"></noscript></p>
                        <p>3. Pada tampilan selanjutnya akan muncul pilihan mengenai komponen mana dari XAMPP yang ingin dan tidak ingin Anda instal. Beberapa pilihan seperti Apache dan PHP adalah bagian penting untuk menjalankan website dan akan otomatis diinstal. Silakan centang MySQL dan phpMyAdmin, untuk pilihan lainnya biarkan saja.</p>
                        <p><img class="aligncenter wp-image-3691 size-full lazyloaded" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-di-windows-7.png" alt="cara instal xampp di windows 7" width="495" height="412" sizes="(max-width: 495px) 100vw, 495px" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-di-windows-7.png 495w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-di-windows-7-300x250.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-di-windows-7-400x333.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-di-windows-7-200x166.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-di-windows-7-50x42.png 50w" data-ll-status="loaded"><noscript><img class="aligncenter wp-image-3691 size-full" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-di-windows-7.png" alt="cara instal xampp di windows 7" width="495" height="412" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-di-windows-7.png 495w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-di-windows-7-300x250.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-di-windows-7-400x333.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-di-windows-7-200x166.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-instal-xampp-di-windows-7-50x42.png 50w" sizes="(max-width: 495px) 100vw, 495px"></noscript></p>
                        <p>4. Berikutnya silakan pilih folder tujuan dimana XAMPP ingin Anda instal, pada tutorial ini pada direktori <em>C:\xampp</em>.</p>
                        <p><img class="aligncenter wp-image-3692 size-full lazyloaded" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-7.png" alt="instal xampp di windows 7" width="500" height="412" sizes="(max-width: 500px) 100vw, 500px" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-7.png 500w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-7-300x247.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-7-400x330.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-7-200x165.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-7-50x41.png 50w" data-ll-status="loaded"><noscript><img class="aligncenter wp-image-3692 size-full" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-7.png" alt="instal xampp di windows 7" width="500" height="412" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-7.png 500w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-7-300x247.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-7-400x330.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-7-200x165.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/instal-xampp-di-windows-7-50x41.png 50w" sizes="(max-width: 500px) 100vw, 500px"></noscript></p>
                        <p>5. Pada halaman selanjutnya, akan ada pilihan apakah Anda ingin menginstal Bitnami untuk XAMPP, dimana nantinya dapat Anda gunakan untuk <a href="https://www.niagahoster.co.id/blog/cara-install-wordpress-di-xampp/" target="_blank" rel="noopener noreferrer">install WordPress,</a> Drupal, dan Joomla seccara otomatis.</p>
                        <p><img class="aligncenter wp-image-3693 size-full lazyloaded" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp.png" alt="cara install xampp" width="503" height="407" sizes="(max-width: 503px) 100vw, 503px" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp.png 503w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp-300x243.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp-500x405.png 500w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp-400x324.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp-200x162.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp-50x40.png 50w" data-ll-status="loaded"><noscript><img class="aligncenter wp-image-3693 size-full" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp.png" alt="cara install xampp" width="503" height="407" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp.png 503w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp-300x243.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp-500x405.png 500w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp-400x324.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp-200x162.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/cara-install-xampp-50x40.png 50w" sizes="(max-width: 503px) 100vw, 503px"></noscript></p>
                        <p>6. Pada langkah ini proses instalasi XAMPP akan dimulai. Silakan klik tombol <b>Next</b>.</p>
                        <p><img class="aligncenter wp-image-3694 size-full lazyloaded" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/menginstal-xampp-e1494822633442.png" alt="menginstal xampp" width="494" height="363" sizes="(max-width: 494px) 100vw, 494px" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/menginstal-xampp-e1494822633442.png 494w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/menginstal-xampp-e1494822633442-300x220.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/menginstal-xampp-e1494822633442-400x294.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/menginstal-xampp-e1494822633442-200x147.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/menginstal-xampp-e1494822633442-50x37.png 50w" data-ll-status="loaded"><noscript><img class="aligncenter wp-image-3694 size-full" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/menginstal-xampp-e1494822633442.png" alt="menginstal xampp" width="494" height="363" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/menginstal-xampp-e1494822633442.png 494w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/menginstal-xampp-e1494822633442-300x220.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/menginstal-xampp-e1494822633442-400x294.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/menginstal-xampp-e1494822633442-200x147.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/menginstal-xampp-e1494822633442-50x37.png 50w" sizes="(max-width: 494px) 100vw, 494px"></noscript></p>
                        <p>7. Setelah berhasil diinstal, akan muncul notifikasi untuk langsung menjalankan control panel. Silakan klik <b>Finish</b>.</p>
                        <p><img class="aligncenter wp-image-3696 size-full lazyloaded" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/control-panel-e1494822793451.png" alt="instalasi xampp selesai" width="491" height="417" sizes="(max-width: 491px) 100vw, 491px" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/control-panel-e1494822793451.png 491w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/control-panel-e1494822793451-300x255.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/control-panel-e1494822793451-400x340.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/control-panel-e1494822793451-200x170.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/control-panel-e1494822793451-50x42.png 50w" data-ll-status="loaded"><noscript><img class="aligncenter wp-image-3696 size-full" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/control-panel-e1494822793451.png" alt="instalasi xampp selesai" width="491" height="417" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/control-panel-e1494822793451.png 491w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/control-panel-e1494822793451-300x255.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/control-panel-e1494822793451-400x340.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/control-panel-e1494822793451-200x170.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/control-panel-e1494822793451-50x42.png 50w" sizes="(max-width: 491px) 100vw, 491px"></noscript></p>
                        <h3><span id="3_Langkah_3_Jalankan_XAMPP"><b>3. Langkah 3: Jalankan XAMPP</b></span></h3>
                        <p>Silakan buka aplikasi XAMPP kemudian klik tombol Start pada Apache dan MySQL. Jika berhasil dijalankan, Apache dan MySQL akan berwarna hijau seperti gambar di bawah ini.</p>
                        <p><img class="aligncenter wp-image-3697 lazyloaded" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan.png" alt="xampp dijalankan" width="491" height="303" sizes="(max-width: 491px) 100vw, 491px" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan.png 732w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan-300x185.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan-680x419.png 680w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan-500x308.png 500w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan-400x246.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan-200x123.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan-50x31.png 50w" data-ll-status="loaded"><noscript><img class="aligncenter wp-image-3697" title="cara instal xampp" src="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan.png" alt="xampp dijalankan" width="491" height="303" srcset="https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan.png 732w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan-300x185.png 300w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan-680x419.png 680w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan-500x308.png 500w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan-400x246.png 400w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan-200x123.png 200w, https://www.niagahoster.co.id/blog/wp-content/uploads/2017/05/xampp-dijalankan-50x31.png 50w" sizes="(max-width: 491px) 100vw, 491px"></noscript></p>
                        <p>Untuk melakukan pengecekan, silakan akses link berikut melalui browser Anda <i>http://localhost</i>.</p>

                    </article>
                </div>
                <article class="article-post">
                    <p>XAMPP merupakan solusi bagi Anda yang ingin menjalankan web server dan database di localhost komputer.&nbsp;</p>
                    <h2 id="h-komponen-penting-pada-xampp"><span id="Komponen_Penting_Pada_XAMPP">Komponen Penting Pada XAMPP</span></h2>
                    <p>Setelah Anda berhasil menginstall XAMPP, Anda bisa menggunakan XAMPP untuk berbagai keperluan develop website. Tapi sebelum itu Anda harus tau beberapa menu penting yang akan sering Anda gunakan ketika menjalankan XAMPP.</p>
                    <h3 id="h-1-config"><span id="1_Config">1. Config</span></h3>
                    <p>Pada menu Config, Anda bisa melakukan beberapa konfigurasi dasar untuk penggunaan XAMPP seperti mengganti:</p>
                    <ul>
                        <li><strong>Editor</strong>: yang akan digunakan untuk mengubah default text editor.</li>
                        <li><strong>Browser</strong>: browser default yang akan digunakan untuk akses web server.</li>
                    </ul>
                    <figure class="wp-block-image"><img src="https://lh5.googleusercontent.com/tulw97b3qOetREXuT8QPV1AYCr1BS0h8K_5bOYm7ZNMCZQ-Ctbzb8OV646OaAZLFkf_nFsmdvz_Agez4TuVE0mmeILPNg8E34_KA5NyYT32SKLTq-ieL3lIyjkNHW8R6_2mTY9Az" alt="" class="lazyloaded" data-ll-status="loaded"><noscript><img src="https://lh5.googleusercontent.com/tulw97b3qOetREXuT8QPV1AYCr1BS0h8K_5bOYm7ZNMCZQ-Ctbzb8OV646OaAZLFkf_nFsmdvz_Agez4TuVE0mmeILPNg8E34_KA5NyYT32SKLTq-ieL3lIyjkNHW8R6_2mTY9Az" alt=""></noscript></figure>
                    <h3 id="h-2-netstat"><span id="2_Netstat">2. Netstat</span></h3>
                    <p>Netstat sering digunakan untuk memastikan apakah ada program/aplikasi lain yang menggunakan port default XAMPP.</p>
                    <figure class="wp-block-image"><img src="https://lh6.googleusercontent.com/ZA745dPehHsBjEgRNMeu-XhqhkzKjdGiVDfzena4blM7K179YFsSmPn9p22u1aMkrYrGCho3gE9pHp2k3XlIRAI0V211w0lVXKCmDnq5F-AvYRkWsTvlMtIU2c3lQJmAxKwlthmr" alt="" class="lazyloaded" data-ll-status="loaded"><noscript><img src="https://lh6.googleusercontent.com/ZA745dPehHsBjEgRNMeu-XhqhkzKjdGiVDfzena4blM7K179YFsSmPn9p22u1aMkrYrGCho3gE9pHp2k3XlIRAI0V211w0lVXKCmDnq5F-AvYRkWsTvlMtIU2c3lQJmAxKwlthmr" alt=""></noscript></figure>
                    <h3 id="h-3-shell"><span id="3_Shell">3. Shell</span></h3>
                    <p>Menu shell pada XAMPP, digunakan untuk menampilkan command prompt pada Windows. Fungsinya untuk melakukan konfigurasi web server atau memperbaiki error pada web server.</p>
                    <figure class="wp-block-image"><img src="https://lh4.googleusercontent.com/PrQSI2V08fK00SBhrHCAEFYjqVAOqgKDeRrvWd1D7-EOaIroCO1W7AI2-lwlEw7Nzt-YNqS5EI9yfHGl_7ztsKxol0yZlbRtKO76NbswG8EyuMd85HhUHhwtPRENbRxqB8u92uFJ" alt="" class="lazyloaded" data-ll-status="loaded"><noscript><img src="https://lh4.googleusercontent.com/PrQSI2V08fK00SBhrHCAEFYjqVAOqgKDeRrvWd1D7-EOaIroCO1W7AI2-lwlEw7Nzt-YNqS5EI9yfHGl_7ztsKxol0yZlbRtKO76NbswG8EyuMd85HhUHhwtPRENbRxqB8u92uFJ" alt=""></noscript></figure>
                    <h3 id="h-4-explorer"><span id="4_Explorer">4. Explorer</span></h3>
                    <p>Explorer merupakan tombol shortcut untuk menuju ke lokasi instalasi XAMPP. Ketika Anda klik icon <strong>Explorer</strong> maka akan diarahkan ke lokasi file instalasi XAMPP.</p>
                    <figure class="wp-block-image"><img src="https://lh3.googleusercontent.com/YzGDwnXZ_3NgUDDTeHrxy4JIls7hfCyNi-29x5sc9QdCEVr4XfterVKIoA8BkdVPy8AMUiEvB8IMgUpA2rRTZiPEGvkWaJWG8XmGyJGpL83RP15J8fnEfWIhsfJTh1VOskEiCvhg" alt="" class="lazyloaded" data-ll-status="loaded"><noscript><img src="https://lh3.googleusercontent.com/YzGDwnXZ_3NgUDDTeHrxy4JIls7hfCyNi-29x5sc9QdCEVr4XfterVKIoA8BkdVPy8AMUiEvB8IMgUpA2rRTZiPEGvkWaJWG8XmGyJGpL83RP15J8fnEfWIhsfJTh1VOskEiCvhg" alt=""></noscript></figure>
                    <h2 id="h-cara-menggunakan-xampp"><span id="Cara_Menggunakan_XAMPP">Cara Menggunakan XAMPP</span></h2>
                    <p>Setelah Anda tahu tentang komponen komponen apa saja yang ada di dalam XAMPP, selanjutnya Anda akan belajar cara menggunakan XAMPP di Windows. Berikut langkahnya.</p>
                    <h3 id="h-1-cara-menjalankan-xampp"><span id="1_Cara_Menjalankan_XAMPP">1. Cara Menjalankan XAMPP</span></h3>
                    <p>Untuk menjalankan XAMPP, pertama buka aplikasi XAMPP yang telah Anda install. Lalu klik <strong>Start</strong> pada module Apache dan MySQL.</p>
                    <figure class="wp-block-image"><img src="https://lh5.googleusercontent.com/IjXZl5Zjdrm87INOSDbxb0yuy5cruPXZ_pqUrIo3DI0pXKLfWzwhMjspspXGn97uk7l6fALY3BEXpJMFCDolLmMtCqDIEOIC3GCJsY-FvlwY9ZNm33QcTSmOl21fU-iwuKFknSs_" alt="" class="lazyloaded" data-ll-status="loaded"><noscript><img src="https://lh5.googleusercontent.com/IjXZl5Zjdrm87INOSDbxb0yuy5cruPXZ_pqUrIo3DI0pXKLfWzwhMjspspXGn97uk7l6fALY3BEXpJMFCDolLmMtCqDIEOIC3GCJsY-FvlwY9ZNm33QcTSmOl21fU-iwuKFknSs_" alt=""></noscript></figure>
                    <p>Setelah keduanya berjalan tanpa error, silahkan akses localhost menggunakan link berikut:</p>
                    <pre class="wp-block-code prettyprinted" style=""><code class=" prettyprinted" style=""><span class="pln"><span class="pln">http</span></span><span class="pun"><span class="pun">:</span></span><span class="com"><span class="com">//localhost</span></span></code></pre>
                    <p>Atau</p>
                    <pre class="wp-block-code prettyprinted" style=""><code class=" prettyprinted" style=""><span class="lit"><span class="lit">127.0</span></span><span class="pun"><span class="pun">.</span></span><span class="lit"><span class="lit">0.1</span></span></code></pre>
                    <p>Maka Anda akan diarahkan ke halaman dashboard XAMPP, seperti pada gambar di bawah ini.</p>
                    <figure class="wp-block-image"><img src="https://lh4.googleusercontent.com/en5WHGSRWKws5ZWs6vC2Gis5h4FTqyY0KTFC3o1Nd7_gqRguCgaCHOwtsu-sFm73TwAD1PE-5KJFMaj7OTakyAC01Sq4Oj3u6aLavdiAWkdDEp4pTDGX8NAaQVcXyVpcSlTVei8s" alt="" class="lazyloaded" data-ll-status="loaded"><noscript><img src="https://lh4.googleusercontent.com/en5WHGSRWKws5ZWs6vC2Gis5h4FTqyY0KTFC3o1Nd7_gqRguCgaCHOwtsu-sFm73TwAD1PE-5KJFMaj7OTakyAC01Sq4Oj3u6aLavdiAWkdDEp4pTDGX8NAaQVcXyVpcSlTVei8s" alt=""></noscript></figure>
                    <p>Langkah-langkah diatas adalah dasar menggunakan XAMPP, pada langkah berikutnya kami akan memberikan cara menggunakan XAMPP untuk install WordPress dan Laravel.</p>
                    <h3 id="h-2-cara-install-wordpress-menggunakan-xampp">
                        <span id="2_Cara_Install_WordPress_Menggunakan_XAMPP">2. Cara Install Aplikasi Kasir </span></h3>
                    <p>Download File Aplikasi Kasir yang berada di halaman depan </p>
                    <br>
                    <img src="<?= base_url('resources/dist/img/home_download.png') ?>">
                    <p>Extract dan taruh file aplikasi kasir di folder explorer didalam folder <strong>htdocs</strong></p>
                    <img src="https://lh3.googleusercontent.com/YzGDwnXZ_3NgUDDTeHrxy4JIls7hfCyNi-29x5sc9QdCEVr4XfterVKIoA8BkdVPy8AMUiEvB8IMgUpA2rRTZiPEGvkWaJWG8XmGyJGpL83RP15J8fnEfWIhsfJTh1VOskEiCvhg" alt="" class="lazyloaded" data-ll-status="loaded">
                    <p>Buka Browser ketik http://localhost/phpmyadmin </p>
                    <img src="<?= base_url('resources/dist/img/create_db_1.png') ?>" alt="event">

                    <p>Pilih New untuk membuat database</p>
                    <img src="<?= base_url('resources/dist/img/create_db_2.png') ?>" alt="event">
                    <p>Kemudian buat nama database dengan nama <strong>pos_umkm</strong> Kemudian klik tombol <strong>create</strong> Maka anda sudah berhasil membuat database ,Selanjutnya import database aplikasi kasir ke database yang kita buat barusan dengan cara seperti ini</p>
                    <img src="<?= base_url('resources/dist/img/import_dbs.png') ?>" alt="event">
                
                    <img src="<?= base_url('resources/dist/img/import_1.png') ?>" alt="event">
                    <img src="<?= base_url('resources/dist/img/import_2.png') ?>" alt="event">
                    <p>Setelah proses import database berhasil sekarang waktunya membuka aplikasi anda dengan cara mengakses browser</p>
                    <pre class="wp-block-code prettyprinted" style=""><code class=" prettyprinted" style=""><span class="pln"><span class="pln">http</span></span><span class="pun"><span class="pun">:</span></span><span class="com"><span class="com">//localhost/pos[nama kasir anda]</span></span></code></pre>
<p>Kemudian Login isikan username :admin dan password:admin </p>
<img src="<?= base_url('resources/dist/img/halaman_apps_login.png') ?>" alt="event">
<img src="<?= base_url('resources/dist/img/halaman_apps_depan.png') ?>" alt="event">
                </article>
            </div>



        </div>
    </section>
    <!--event section end -->
    <!--footer start -->
    <footer>
        <div class="container">
            <div class="row justify-content-center">





            </div>
        </div>
    </footer>
    <div class="copyright_footer">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-12">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | Mobile Kasir</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>

            </div>
        </div>
    </div>
    <!--footer end -->

    <!-- jquery -->
    <script src="<?php echo base_url("resources/assets/js/jquery.min.js") ?>"></script>
    <!-- bootstrap -->
    <script src="<?php echo base_url("resources/assets/js/popper.js") ?>"></script>
    <script src="<?php echo base_url("resources/assets/js/bootstrap.min.js") ?>"></script>
    <script src="<?php echo base_url("resources/assets/js/waypoints.min.js") ?>"></script>
    <!--slick carousel -->
    <script src="<?php echo base_url("resources/assets/js/owl.carousel.min.js") ?>"></script>
    <!--parallax -->
    <script src="<?php echo base_url("resources/assets/js/parallax.min.js") ?>"></script>
    <!--Counter up -->
    <script src="<?php echo base_url("resources/assets/js/jquery.counterup.min.js") ?>"></script>
    <!--Counter down -->
    <script src="<?php echo base_url("resources/assets/js/jquery.countdown.min.js") ?>"></script>
    <!-- WOW JS -->
    <script src="<?php echo base_url("resources/assets/js/wow.min.js") ?>"></script>
    <!-- Custom js -->
    <script src="<?php echo base_url("resources/assets/js/main.js") ?>"></script>
</body>

</html>